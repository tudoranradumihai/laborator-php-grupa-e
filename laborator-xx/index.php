<?php
var_Dump($_SERVER);
$from = json_decode(file_get_contents("http://maps.google.com/maps/api/geocode/json?address=".urlencode($_GET["from"])),TRUE);
if($from["status"]=="OK"){
	$from = reset($from["results"]);
	$_GET["from"] = $from["formatted_address"];
	$JSONFrom = array(
		"lat" =>$from["geometry"]["location"]["lat"],
		"lng" =>$from["geometry"]["location"]["lng"]
	);
}
$to = json_decode(file_get_contents("http://maps.google.com/maps/api/geocode/json?address=".urlencode($_GET["to"])),TRUE);
if($to["status"]=="OK"){
	$to = reset($to["results"]);
	$_GET["to"] = $to["formatted_address"];
	var_dump($to["geometry"]["location"]["lat"]);
	var_dump($to["geometry"]["location"]["lng"]);
	$JSONTo = array(
		"lat" =>$to["geometry"]["location"]["lat"],
		"lng" =>$to["geometry"]["location"]["lng"]
	);
}



// https://developers.google.com/maps/documentation/distance-matrix/intro
?>
<form>
	<input type="text" name="from" value="<?php echo array_key_exists("from", $_GET) ? $_GET["from"] : "" ?>">
	<input type="text" name="to" value="<?php echo array_key_exists("to", $_GET) ? $_GET["to"] : "" ?>">
	<input type="submit">
</form>
<div id="map"></div>
<script>

  function initMap() {
  <?php if(isset($JSONFrom)) { ?>
    var from = <?php echo json_encode($JSONFrom);?>;	
<?php } ?>
  <?php if(isset($JSONTo)) { ?>
    var to = <?php echo json_encode($JSONTo);?>;	
<?php } ?>
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 6,
      center: from
    });

<?php if(isset($JSONFrom)) { ?>
    var markerFrom = new google.maps.Marker({
      position: from,
      map: map,
      title: 'Hello World!'
    });
<?php } ?>
<?php if(isset($JSONTo)) { ?>
    var markerTo = new google.maps.Marker({
      position: to,
      map: map,
      title: 'Hello World!'
    });
<?php } ?>
  }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB55OBdUfu0UorG9iamtK5fch2Y1c2iAPU&callback=initMap"></script>
<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
