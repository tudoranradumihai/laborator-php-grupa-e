<?php
$phrase = "A small step for a man, a big step for the human kind";
//$phrase = "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.";

$phrase = str_replace(array(".",",","!"),"",$phrase);
$array = explode(" ",$phrase);
$newArray = array();
foreach($array as $word){
	$temporary = file_get_contents("https://api.datamuse.com/words?rel_syn=$word");
	$temporary = json_decode($temporary,TRUE);
	if(count($temporary)>0){
		$temporary = reset($temporary);
		array_push($newArray,$temporary["word"]);
	} else {
		array_push($newArray,$word);
	}
}
echo implode(" ",$newArray);