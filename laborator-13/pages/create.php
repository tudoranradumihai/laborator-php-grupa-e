<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
	$validation = true;
	$errorMessages = array();
	$savedFields = array();
	$requiredFields = array("firstname","lastname","cnp","email");
	foreach($requiredFields as $field){
		if(trim($_POST[$field])==""){
			$validation = false;
			array_push($errorMessages,"Field '$field' is required.");
		} else {
			$savedFields[$field] = $_POST[$field];
		}
	}
	if(trim($_POST["password"])==""){
		$validation = false;
		array_push($errorMessages,"Field 'password' is required.");
	}
	if($_POST["cnp"]!=""){
		if(strlen($_POST["cnp"])!=13){
			$validation = false;
			if(array_key_exists("cnp", $savedFields)){
				unset($savedFields["cnp"]);
			}
			array_push($errorMessages,"Field 'cnp' must have 13 numeric characters.");
		} else if(!is_numeric($_POST["cnp"])){
			$validation = false;
			if(array_key_exists("cnp", $savedFields)){
				unset($savedFields["cnp"]);
			}
			array_push($errorMessages,"Field 'cnp' must have numeric characters.");
		}
	}
	if($_POST["email"]!=""){
		if(!preg_match('/^([a-z0-9]+([_\.\-]{1}[a-z0-9]+)*){1}([@]){1}([a-z0-9]+([_\-]{1}[a-z0-9]+)*)+(([\.]{1}[a-z]{2,6}){0,3}){1}$/i', $_POST["email"])){
			$validation = false;
			if(array_key_exists("email", $savedFields)){
				unset($savedFields["email"]);
			}
			array_push($errorMessages,"Field 'email' must have a valid email format.");
		} else {
			// not ok to make multiple mysql connections, need to find a solution - HOMEWORK
			$connection = mysqli_connect("localhost","root","","laborator-14");
			if($connection){
				$query = "SELECT * FROM users WHERE email LIKE '".mysqli_escape_string($connection,$_POST["email"])."' AND deleted=0;";
				$result = mysqli_query($connection,$query);
				if(mysqli_num_rows($result)!=0){
					$validation = false;
					if(array_key_exists("email", $savedFields)){
						unset($savedFields["email"]);
					}
					array_push($errorMessages,"The email '$_POST[email]' already exists in the database.");
				}
				mysqli_close($connection);
			}
		}
	}
	if($_POST["password"]!=""){
		if(strlen($_POST["password"])<8){
			$validation = false;
			array_push($errorMessages,"You password must have at least 8 characters.");
		} else if(trim($_POST["password2"])==""){
			$validation = false;
			array_push($errorMessages,"You must confirm your password.");
		} else if ($_POST["password"]!=$_POST["password2"]){
			$validation = false;
			array_push($errorMessages,"Your passwords don't match.");
		}
	}
	if ($validation){
		$connection = mysqli_connect("localhost","root","","laborator-14");
		if($connection){
			$user = array(
				"firstname" => mysqli_escape_string($connection,$_POST["firstname"]),
				"lastname" => mysqli_escape_string($connection,$_POST["lastname"]),
				"cnp" => mysqli_escape_string($connection,$_POST["cnp"]),
				"email" => mysqli_escape_string($connection,$_POST["email"]),
				"password" => md5(mysqli_escape_string($connection,$_POST["password"])),
			);
			$query = "INSERT INTO users (firstname,lastname,cnp,email,password) VALUES ('$user[firstname]','$user[lastname]','$user[cnp]','$user[email]','$user[password]');";

			$result = mysqli_query($connection,$query);
			if($result){
				$message = array(
					"description" => "The new user was created.",
					"class" => "success"
				);
			} else {
				$message = array(
					"description" => "The new user was not created.",
					"class" => "danger"
				);
			}
			mysqli_close($connection);
		} else {
			$message = array(
				"description" => "MySQL connection ERROR.",
				"class" => "warning"
			);
		}
	} else {
		$_SESSION["errorMessages"] = $errorMessages;
		$_SESSION["savedFields"] = $savedFields;
		header("Location: index.php?page=new");
	}
} else {
	header("Location: index.php?page=new");
}
?>
<div class="alert alert-<?php echo $message["class"] ?>" role="alert">
<?php echo $message["description"] ?>
</div>
