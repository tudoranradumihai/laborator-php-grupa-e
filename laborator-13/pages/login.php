<?php
if(array_key_exists("user", $_COOKIE)){
	header("Location: index.php?page=profile");
}
?>
<div class="row justify-content-md-center">
	<div class="col-md-4">
		<h1>Connect</h1>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus enim ligula, a ullamcorper odio sodales eu.
			<?php
				if(array_key_exists("errors", $_SESSION)){
					foreach($_SESSION["errors"] as $error){
						echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
					}
					unset($_SESSION["errors"]);
				}
			?>
			<form id="login" action="index.php?page=connect" method="POST">
			  <div class="form-group">
			    <label for="email">Email address</label>
			    <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Enter email">
			    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			  </div>
			  <div class="form-group">
			    <label for="password">Password</label>
			    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
			  </div>
			  <button type="submit" class="btn btn-primary">Connect</button>
			</form>
		</p>
	</div>
</div>