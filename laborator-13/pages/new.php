<?php
if(array_key_exists("user", $_COOKIE)){
	header("Location: index.php?page=profile");
}

function checkField($field){
	if(array_key_exists("savedFields", $_SESSION)){
		if(array_key_exists($field, $_SESSION["savedFields"])){
			echo $_SESSION["savedFields"][$field];
			unset($_SESSION["savedFields"][$field]);
		}
	}
}
?>
<section id="new">
	<h1>Create User</h1>
	<?php
		if(array_key_exists("errorMessages", $_SESSION)){
			foreach($_SESSION["errorMessages"] as $error){
				echo '<div class="alert alert-danger" role="alert">'.$error.'</div>';
			}
			unset($_SESSION["errorMessages"]);
		}
	?>
	<form method="POST" id="new-user" action="index.php?page=create">
		<div class="form-row">
			<div class="form-group col-md-12">
				<h2>Personal Information</h2>
			</div>
			<div class="form-group col-md-6">
				<label for="firstname">Firstname</label>
				<input type="text" name="firstname" id="firstname" class="form-control" placeholder="John" value="<?php echo checkField("firstname") ?>">
			</div>
			<div class="form-group col-md-6">
				<label for="lastname">Lastname</label>
				<input type="text" name="lastname" id="lastname" class="form-control" placeholder="Doe"value="<?php echo checkField("lastname") ?>">
			</div>
			<div class="form-group col-md-12">
				<h2>Aditional Information</h2>
			</div>
			<div class="form-group col-md-6">
				<label for="cnp">CNP</label>
				<input type="text" name="cnp" id="cnp" class="form-control" placeholder="1851001123456" value="<?php echo checkField("cnp") ?>">
			</div>
			<div class="form-group col-md-12">
				<h2>Login Information</h2>
			</div>
			<div class="form-group col-md-12">
				<label for="email">Email Address</label>
				<input type="text" name="email" id="email" class="form-control" placeholder="john.doe@domain.com" value="<?php echo checkField("email") ?>">
			</div>
			<div class="form-group col-md-6">
				<label for="password">Password</label>
				<input type="password" name="password" id="password" class="form-control">
			</div>
			<div class="form-group col-md-6">
				<label for="password2">Confirm Password</label>
				<input type="password" name="password2" id="password2" class="form-control">
			</div>
			<div class="form-group col-md-12">
				<button type="submit" class="btn btn-primary">Create User</button>
			</div>
		</div>
	</form>
</section>