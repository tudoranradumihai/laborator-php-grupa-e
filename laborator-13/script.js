$(document).ready(function(){
	//alert("This is an alert message.");
	$(".description").hide();
	$("#show-description").click(function(){
		$("#header-description").show();
	});

	$("#new-user").validate({
		rules: {
			firstname: "required",
			lastname: "required",
			cnp: {
				required: true,
				digits: true,
				minlength: 13,
				maxlength: 13
			},
			email: {
				required: true,
				email: true,
				remote: {
	                url: "check-email.php",
	                type: "post",
	                data: {
	                    email: function() {
	                        return $("#email").val();
	                    }
	                }
	            }
			}
		},
		messages: {
			email: {
				remote: "Email address already in use. Please use other email."
			}
		},
		submitHandler: function(form) {
			$("#new-user").submit();
	    }
	}); 	

	$("#login").validate({
		rules: {
			email: {
				required: true,
				email: true,
				remote: {
					url: "verify-email.php",
					method: "post",
					data: {
						email: function() {
							return $("#email").val();
						}
					}
				}
			},
			password: {
				required: true,
				minlength: 8
			}
		},
		messages: {
			email: {
				remote: "The email doesn't exists in the database."
			}
		},
		submitHandler: function(form) {
			$("#login").submit();
	    }
	});
});