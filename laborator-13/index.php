<?php
	session_start();

	$pages = array("profile");
	if(array_key_exists("page", $_GET)){
		foreach($pages as $page){
			if($_GET["page"]==$page){
				if(array_key_exists("user", $_COOKIE)){
					if(!is_null($_COOKIE["user"]) && is_numeric($_COOKIE["user"])){
						setcookie("user",$_COOKIE["user"],time()+3600);
						if(!array_key_exists("user", $_SESSION)){
							/* ... */
							$query = "SELECT * FROM users WHERE id=".$_COOKIE["user"];
							/* ... */
						}
					} else {
						header("Location: index.php?page=login");
					}
				} else {
					header("Location: index.php?page=login");
				}
			}
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>My Website</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<header>
				<h1>Title</h1>
				<a id="show-description">Show Description</a>
				<p class="description" id="header-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer cursus enim ligula, a ullamcorper odio sodales eu. Donec efficitur finibus mollis. Phasellus ac consequat urna, a tincidunt libero. Curabitur a ligula dolor. Curabitur sit amet mauris nulla. Phasellus ac sapien in elit scelerisque posuere. Sed elit tellus, pulvinar in magna sed, hendrerit consequat nisl. Fusce volutpat malesuada eros eget pellentesque. Curabitur iaculis, quam laoreet pharetra mattis, nisl nibh posuere quam, ut pulvinar mi purus sed ligula. Donec efficitur condimentum accumsan. Ut congue bibendum est. Mauris sit amet egestas orci, condimentum lobortis purus. Nulla facilisi. Aliquam id sapien mauris. Nulla tempus, arcu eget rutrum accumsan, neque neque tempor ipsum, at vestibulum est risus a ligula.</p>
				<br>
				<?php if(array_key_exists("user", $_COOKIE)) { ?>
				Buna ziua <?php echo $_SESSION["user"]["firstname"]." ".$_SESSION["user"]["lastname"] ?> |
				<a href="index.php?page=disconnect">Logout</a>
				<?php } else { ?>
				<a href="index.php?page=login">Login</a> |
				<a href="index.php?page=new">Register</a>
				<?php } ?>
				<nav class="navbar navbar-expand-lg navbar-light bg-light">
				  <a class="navbar-brand" href="#">Menu</a>
				  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span class="navbar-toggler-icon"></span>
				  </button>

				  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				    <ul class="navbar-nav mr-auto">
				      <li class="nav-item active">
				        <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
				      </li>
				    </ul>
				    <form class="form-inline my-2 my-lg-0">
				      <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
				      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
				    </form>
				  </div>
				</nav>
			</header>
			<main>
			<?php
				if(array_key_exists("page", $_GET)){
					$filepath = "pages/$_GET[page].php";
					if(file_exists($filepath)){
						include $filepath;
					} else {
						echo "ERROR: File $filepath doesn't exists!";
					}
				} else {
					include "pages/homepage.php";
				}
			?>
			</main>
		</div>
		<script src="https://code.jquery.com/jquery-3.1.1.js"></script>
		<!--
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		-->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.js"></script>
		<script src="script.js"></script>
	</body>
</html>