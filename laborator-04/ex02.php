<?php
// http://php.net/manual/en/ref.strings.php

$string = "lorem ipsum dolor sit amet";

echo "Stringul este: ".$string."<br>";
// http://php.net/manual/en/function.strlen.php
echo "Lungimea stringului este: ".strlen($string)."<br>";
// http://php.net/manual/en/function.count.php
// http://php.net/manual/en/function.explode.php
echo "Stringul are ".count(explode(" ",$string))." cuvinte<br>";

$string = "lorem ipsum dolor sit amet   ";
echo "Stringul este: ".$string."<br>";
echo "Lungimea stringului este: ".strlen($string)."<br>";
// http://php.net/manual/en/function.trim.php
// http://php.net/manual/en/function.ltrim.php
// http://php.net/manual/en/function.rtrim.php
$string = trim($string);
echo "Lungimea stringului este: ".strlen($string)."<br><br>";

$array = array("John","Jack","James");
echo implode(", ",$array)."<br><br>";

$string = "lorem ipsum Dolor sit amet";
echo "Stringul este: ".$string."<br>";
echo strtoupper($string)."<br>";		
echo strtolower($string)."<br>";	
echo ucfirst($string)."<br>";
echo ucwords($string)."<br>";
echo lcfirst($string)."<br>";

/*
$string1 = "literaly";
$string2 = "literally";
echo levenshtein($string1, $string2)."<br>";
echo similar_text($string1, $string2)."<br>";
echo strlen($string1)."<br>";
*/

$message = "Welcome\nTo the other side";
echo nl2br($message)."<br>";

$number = 9999.9876;
echo $number."<br>";
echo number_format($number)."<br>";
echo number_format($number,2)."<br>";
echo number_format($number,2," lei si "," ")." bani <br>";

$string = "ana are mere si adriana are prune";
echo str_replace("mere","pere",$string)."<br>";
echo str_replace(array("mere","prune"),"fructe",$string)."<br>";