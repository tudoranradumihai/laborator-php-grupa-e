<?php
// am declarat un array de array-uri cu date despre produse
$products = array(
	array("name"=>"iPhone","price"=>1000),
	array("name"=>"Samsung","price"=>800),
	array("name"=>"Huawei","price"=>700),
	array("name"=>"Miria","price"=>25),
	array("name"=>"Nokia","price"=>150),
);

// am declarat un formular cu metoda de transmitere GET si cu un singur input text name=search
// in valoare inputului vom afisa valoare $_GET[search] doar daca cheia search exista in $_GET
?>
<form method="GET">
Text:<input type="text" name="search" value="<?php if(array_key_exists("search",$_GET)) { echo $_GET["search"]; } ?>" /><Br>
Min Price:<input type="text" name="minPrice"  value="<?php if(array_key_exists("minPrice",$_GET)) { echo $_GET["minPrice"]; } ?>"  /><br>
Max Price:<input type="text" name="maxPrice"  value="<?php if(array_key_exists("maxPrice",$_GET)) { echo $_GET["maxPrice"]; } ?>"  /><br>
<input type="submit" />
</form>
<?php
// daca $_GET["search"] este introdus se afiseaza un mesaj
if (array_key_exists("search", $_GET)){
	echo "Cuvantul cautat de dvs. este: <b>$_GET[search]</b><br><br>Produse<br>";
}

/*
// se face o bucla de parcurgere a variabilei $products
foreach ($products as $product) {

	// se verifica daca s-au aplicat filtre
	if (array_key_exists("search", $_GET)){
		// daca s-au aplicat filtre se verifica daca textul din filtru exista in numele produsului si se afiseaza daca acesta exista sau daca textul este vid
		if (stripos($product["name"],$_GET["search"])!==false|| $_GET["search"]==""){
			echo $product["name"].": ".$product["price"]."&euro;<br>";
		}
	} else {
		// daca nu s-au aplicat filtre, se afiseaza produsele
		echo $product["name"].": ".$product["price"]."&euro;<br>";
	}
}
*/

foreach ($products as $product) {
	$validation = true;
	
	if(array_key_exists("search", $_GET)){
		if ($_GET["search"]!=""){
			if (stripos($product["name"],$_GET["search"])===false){
				$validation = false;
			}
		}
	}
	if(array_key_exists("minPrice", $_GET)){
		if ($_GET["minPrice"]!=""){
			if (floatval($product["price"])<floatval($_GET["minPrice"])){
				$validation = false;
			}
		}
	}
	if(array_key_exists("maxPrice", $_GET)){
		if ($_GET["maxPrice"]!=""){
			if (floatval($product["price"])>floatval($_GET["maxPrice"])){
				$validation = false;
			}
		}
	}

	if($validation){
		echo $product["name"].": ".$product["price"]."&euro;<br>";
	}
}
?>
