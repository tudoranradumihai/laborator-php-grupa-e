CREATE DATABASE sampledatabase;
USE sampledatabase;

# https://dev.mysql.com/doc/refman/5.7/en/create-table.html
#https://www.w3schools.com/php/php_mysql_intro.asp
CREATE TABLE IF NOT EXISTS users (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,

	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	birthdate DATE,

	createddate DATETIME DEFAULT CURRENT_TIMESTAMP
);

DROP TABLE IF EXISTS users;

INSERT INTO users (id,firstname,lastname,email,birthdate) 
VALUES (1,"FN","LN","FN.LN@gmail.com","1987-10-04");

INSERT INTO users (id,lastname,email,birthdate) 
VALUES (1,"LN","FN.LN@gmail.com","1987-10-04");

INSERT INTO users (firstname,lastname,email,birthdate) 
VALUES ("FN","LN","FN.LN@gmail.com","1987-10-04");


CREATE DATABASE shop;

CREATE TABLE categories(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL
);

CREATE TABLE products(
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description TEXT,
	stock INT(11) DEFAULT 0,
	price FLOAT(9,2),
	category INT(11),
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (category) REFERENCES categories(id)
);



INSERT INTO products 
	(name,description,stock,price)
	VALUES
	("iPhone","Lorem Ipsum Dolor Sit Amet.",5,999.90),
	("iPhone 7","Lorem Ipsum Dolor Sit Amet.",5,899.90),
	("iPhone 6s","Lorem Ipsum Dolor Sit Amet.",5,799.90),
	("iPhone 6","Lorem Ipsum Dolor Sit Amet.",5,699.90),
	("iPhone 5s","Lorem Ipsum Dolor Sit Amet.",5,599.90);

SELECT * FROM products;

SELECT name,price FROM products;

SELECT * FROM products WHERE id=1;
SELECT * FROM products WHERE stock>0;
SELECT * FROM products WHERE price > 700 AND price < 900;
SELECT * FROM products WHERE name LIKE 'iPhone';
SELECT * FROM products WHERE name LIKE 'iPhone%';
SELECT * FROM products WHERE name LIKE '%iPhone';
SELECT * FROM products WHERE name LIKE '%iPhone%';

SELECT * FROM products ORDER BY price ASC;

SELECT * FROM products LIMIT 2 OFFSET 0;

SELECT * FROM products LIMIT 2 OFFSET 2;
SELECT * FROM products LIMIT 2 OFFSET 4;

SELECT name,description,price 
	FROM products 
	WHERE price > 600
		AND name LIKE '%iPhone%'
	ORDER BY price descriere
	LIMIT 1
	OFFSET 1;


tabela continents - id, name 
tabela countries - id, name,continent(FK)
tabela citytype - id,type
tabela cities - id,name,country(FK),type(FK)

5
3,2
5,
4,1

fiecare tabela sa aiba minim 5 intrari facute de mana
