<?php
Class C1{
	public $p1;
	protected $p2;
	private $p3;
	public function m1(){
		echo $this->p1; // DA
		echo $this->p2; // DA
		echo $this->p3; // DA
	}
}
$ob1 = new C1();
echo $ob1->p1; // DA
echo $ob1->p2; // NU
echo $ob1->p3; // NU
Class C2 extends C1 {
	public function m2(){
		echo $this->p1; // DA
		echo $this->p2; // DA
		echo $this->p3; // NU
	}
}
$ob2 = new C2();
echo $ob1->p1; // DA
echo $ob1->p2; // NU
echo $ob1->p3; // NU