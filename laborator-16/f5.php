<?php

Class C1 {
	public $p1;
	public $p2;
	private $p3;
	protected $p4;
	public function m1(){
		return $this->p3; // ii ok
	}
	public function m2(){
		return $this->p4; // ii ok
	}
}

$ob1 = new C1();
var_dump($ob1);

Class C2 extends C1 {
	public $p11;
	public function m11(){
		return $this->p3; // aici da eroare
	}
	public function m12(){
		return $this->p4; //
	}
}

$ob2 = new C2();
var_dump($ob2);