<?php

/*
$array = array(
	"firstname" => "R",
	"lastname" => "T"
);
$array["firstname"] ="CACA";
unset($array["firstname"]);
var_dump($array);
*/

Class NumeClasa {

}

// definire clasa
Class Car {
	
	// definire proprietate publica
	public $color;
	// definire proprietate publica cu valoare predefinita
	public $motor = "2.0";
	// definire metoda publica
	public function method1() {
		/* aici se poate scrie PHP procedural */
		echo "Lorem Ipsum Dolor<br>";
	}

	public function method2(){
		return $this->color;
	}

	public function method3(){
		echo self::method2();
	}

}
// definire obiect din clasa
$object = new Car();
// setare valoare proprietate
$object->color = "red";
// accesare valoare proprietate
echo $object->color."<br>";

$object->method1();

echo $object->method2();

