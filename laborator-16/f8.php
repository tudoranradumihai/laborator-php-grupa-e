<?php

Class Car{
	protected $brand;
	protected $model;
	protected $engine;
	protected $type;
	public function __construct($p1,$p2,$p3,$p4){
		$this->brand = $p1;
		$this->model = $p2;
		$this->engine = $p3;
		$this->type = $p4;
	}
}
// V1
Class Mercedes1 extends Car{
	public function __construct($p1,$p2,$p3){
		$this->brand = "Mercedes";
		$this->model = $p1;
		$this->engine = $p2;
		$this->type = $p3;
	}
}
// V2
Class Mercedes2 extends Car{
	public function __construct($p1,$p2,$p3){
		parent::__construct("Mercedes",$p1,$p2,$p3);
	}
}
Class EClasse extends Mercedes2{
	public function __construct($p1,$p2){
		parent::__construct("E",$p1,$p2);
	}
}

$e = new EClasse("2196","D");
var_dump($e);