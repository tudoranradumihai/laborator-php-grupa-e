<?php
/*
Clasa MyClass
5 proprietati p1 - p5;
p5 va avea valoare default 5
metoda m1 va returna proprietatea p1
metoda m2 care va seta proprietatea p1
*/

Class MyClass {

	public $p1;
	public $p2;
	public $p3;
	public $p4;
	public $p5 = 5;

	public function m1(){
		return $this->p1;
	}

	public function m2($parameter){
		$this->p1 = $parameter;
	}

	public function m3(){
		echo self::m1();
	}
}

$object = new MyClass();
$object->m2(10);
echo $object->m1();