<?php

Class C3 {

	public $p1;
	private $p2;

	public function setP2($p2){
		$this->p2 = $p2;
	}

	public function getP2(){
		return $this->p2;
	}

}

$obj = new C3();
$obj->p1 = 1;
$obj->setP2(2);

var_dump($obj);