<?php
// cicluri


$a = 1;
while ($a < 6){
	echo $a."<br>";
	$a++;
}

$a = 1;
do {
	echo $a."<br>";
	$a++;
} while ($a < 6);

/*
$a = 1;
while ($a < 1){
	echo "1. Intra macar odata<br>";
}

$a = 1;
do {
	echo "2. Intra macar odata<br>";
} while ($a < 1);

while(true){
	echo "Adevaraaaat.";
}
*/

/*
Sa se scrie un ciclu while care afiseaza primele 5 numere divizibile cu 7.
*/

$number = 1;
$counter = 1;

while ($counter <= 5) {
	if($number%7==0){
		echo $number."<br>";
		$counter++;
	}
	$number++;
}

/*
Sa se scrie un ciclu while care afiseaza ultimele 5 numere divizibile cu 3 de 4 cifre.
*/

$number = 9999;
$counter = 1;

while ($counter <= 5){
	if($number%3==0){
		echo $number."<br>";
		$counter++;
	}
	$number--;
}

for($i=1;$i<=5;$i++){
	echo $i."<br>";
}

$a = array(1,2,3,4,5);

foreach($a as $value){
	echo $value."<br>";
}

foreach($a as $key => $value){
	echo '$a['.$key.']='.$value."<br>";
}