<h1>Contact Us</h1>
<form method="POST" action="index.php?page=confirmation">
<div class="row">
<div class="col-md-6"> 
<div class="form-group">
	<label for="firstname">Firstname</label>
	<input type="text" class="form-control" id="firstname" placeholder="John" name="firstname">
</div>
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="lastname">Lastname</label>
	<input type="text" class="form-control" id="lastname" placeholder="Doe" name="lastname">
</div>
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="email">Email Address</label>
	<input type="text" class="form-control" id="email" placeholder="john.doe@domain.com" name="email">
</div>
</div>
<div class="col-md-6">
<div class="form-group">
	<label for="phone">Telephone Number</label>
	<input type="text" class="form-control" id="phone" placeholder="(+40) 712 345 678" name="phone">
</div>
</div>
<div class="col-md-12">
	<label for="message">Message</label>
	<textarea class="form-control" id="message" name="message"></textarea>
</div>
<div class="col-md-12">
	<button type="submit" class="btn btn-primary">Submit</button>
</div>
</div>
</form>
