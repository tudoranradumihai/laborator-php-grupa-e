<?php echo '<!-- '.__FILE__.' -->'.PHP_EOL; ?>
<?php
	$pages = array(
		"homepage" => "Home",
		"contact" => "Contact Us"
	);
?>
	<header>
		<h1>
			Website Title
			<small>Details about the website</small>
		</h1>
		<nav>
			<?php
				if (count($pages)>0){
					echo '<ul class="nav">';
					foreach ($pages as $key => $value) {
						echo '<li class="nav-item">
								<a class="nav-link" href="index.php?page='.$key.'">'.$value.'</a>
				</li>';
					}
					echo '</ul>';
				}
			?>
		</nav>
	</header>