<!DOCTYPE html>
<html>
	<head>
		<?php include "resources/headerResources.php"; ?>
	</head>
	<body>
		<div class="container">
			<?php include "resources/header.php"; ?>
			<?php 
				if(array_key_exists("page", $_GET)){
					$filepath = "pages/".$_GET["page"].".php";
					if(file_exists($filepath)){
						include $filepath;
					} else {
						include "pages/404.php";
					}
				} else {
					include "pages/homepage.php";
				}
			?>
			<?php include "resources/footer.php"; ?>
		</div>
		<?php include "resources/footerResources.php"; ?>
	</body>
</html>