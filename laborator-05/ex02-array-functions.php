<pre>
<?php
// http://php.net/manual/en/ref.array.php
$a1 = array(1,2,3,4,5);

var_dump(is_array($a1));
echo "<br>";
var_dump(is_array("lorem ipsum"));
echo "<br>";
var_dump(array_chunk($a1,2));
echo "<br>";

$a2 = array(
	0 => array(
		"firstname" => "Prenume1",
		"lastname" => "Nume1"
	),
	1 => array(
		"firstname" => "Prenume2",
		"lastname" => "Nume2"
	)
);
var_dump(array_column($a2,"firstname"));
echo "<br>";

$k1 = array("firstname","lastname");
$v1 = array("Prenume","Nume");
var_dump(array_combine($k1, $v1));
echo "<br>";

$a3 = array(1,2,3,4,2,5,2,6);
var_dump(array_count_values($a3));
echo "<br>";

$a4 = array(1,2,3);
$a5 = array(1,2,4);
var_dump(array_diff($a4,$a5));
echo "<br>";

var_dump(array_intersect($a4,$a5));
echo "<br>";

$a6 = array(
	"firstname" => "Radu",
	"lastname"  => "Tudoran"
);
var_dump(array_key_exists("firstname", $a6));
echo "<br>";
var_dump(array_key_exists("middlename", $a6));
echo "<br>";
var_dump(array_keys($a6));
echo "<br>";
var_dump(array_values($a6));
echo "<br>";
var_dump(array_merge($a1,$a2,$a3,$a4,$a5,$a6));
echo "<br>";
var_dump(array_pop($a6));
echo "<br>";
var_dump($a6);
?>
</pre>