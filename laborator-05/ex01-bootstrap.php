<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
					  <div class="carousel-inner">
					    <div class="carousel-item active">
					      <img class="d-block w-100" src="http://webdevelopmenttraining.ro/template/assets/img/sliders/3.jpg" alt="First slide">
					    </div>
					    <div class="carousel-item">
					      <img class="d-block w-100" src="http://webdevelopmenttraining.ro/template/assets/img/sliders/4.jpg" alt="Second slide">
					    </div>
					    <div class="carousel-item">
					      <img class="d-block w-100" src="http://webdevelopmenttraining.ro/template/assets/img/sliders/5.jpg" alt="Third slide">
					    </div>
					  </div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>					
				</div>
				<div class="col-md-8">
					<h1>
						Website Title
						<small class="text-muted">Subtitle</small>
					</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu sapien at dolor commodo egestas. Maecenas eros justo, sodales non porta non, luctus a velit. Praesent nisl felis, sodales vel quam vel, iaculis vehicula massa. Nam auctor eros ultricies lorem consectetur accumsan. Proin id tristique lacus. Donec tortor purus, dignissim id lorem vitae, tristique convallis lacus. Suspendisse elementum porta pellentesque. Vivamus vitae dolor vitae elit blandit molestie ut eu ante. Morbi mattis tincidunt imperdiet. In vitae ultrices mi. Sed rhoncus diam ac nunc iaculis suscipit. Quisque posuere feugiat est. Nullam suscipit lobortis tempus. In eu consectetur nulla.
					</p>
					<div class="card" style="width: 20rem;">
					  <img class="card-img-top" src="..." alt="Card image cap">
					  <div class="card-body">
					    <h4 class="card-title">Card title</h4>
					    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					    <a href="#" class="btn btn-primary">Go somewhere</a>
					  </div>
					</div>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eu sapien at dolor commodo egestas. Maecenas eros justo, sodales non porta non, luctus a velit. Praesent nisl felis, sodales vel quam vel, iaculis vehicula massa. Nam auctor eros ultricies lorem consectetur accumsan. Proin id tristique lacus. Donec tortor purus, dignissim id lorem vitae, tristique convallis lacus. Suspendisse elementum porta pellentesque. Vivamus vitae dolor vitae elit blandit molestie ut eu ante. Morbi mattis tincidunt imperdiet. In vitae ultrices mi. Sed rhoncus diam ac nunc iaculis suscipit. Quisque posuere feugiat est. Nullam suscipit lobortis tempus. In eu consectetur nulla.
					</p>
				<div class="col-md-4">
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
				</div>
				<div class="col-md-3">
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	</body>
</html>