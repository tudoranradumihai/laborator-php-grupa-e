<?php

function __autoload($className){
	echo "ETAPA 2: Verificare __autoload()<br>";
	$folders = array("Classes");
	foreach($folders as $folder){
		$fileName = "$folder/$className.php";
		if(file_exists($fileName)){
			require_once $fileName;
		}
	}
}

echo "ETAPA 1: INITIALIZARE<br>";
$object = new Test();
$object1 = new Test();
$object->method();
unset($object);
echo "AICI E ULTIMA LINIE<br>";