<?php

Class MyClass {

	public function myMethod(){
		self::myStaticMethod();
	}

	public static function myStaticMethod(){

	}
}

MyClass::myStaticMethod();

$object = new MyClass();
$object->myMethod();
$object->myStaticMethod();

MyClass::myStaticMethod();