<?php

Class MyClass {
	/*
	declarare proprietati
	*/
	public $a /* = "value" */;
	protected $b;
	private $c;

	/*
	declarare metode
	*/

	public function __construct($p=NULL){

	}
}

$object = new MyClass();

Class C1 {
	public function m1(){
	}
}

Class C2 extends C1 {
	public function m1(){
		parent::m1();
	}
	public function m2(){
		parent::m1();
	}
} 

Class C3 extends C2 {
	public function m3(){
		parent::m1();
	}
}