<?php
// http://php.net/manual/en/language.oop5.constants.php
Class SimpleClass {
	const MYCONST = "MYVALUE";
	public function showConstant(){
		echo self::MYCONST;
	}
}

echo SimpleClass::MYCONST;
$object = new SimpleClass();
$object->showConstant();