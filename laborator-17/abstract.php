<?php

/* 
Clasa Simpla
- proprietati, constante, metode
*/
Class SimpleClass {

}

/* 
Interfata
- structura metode (nu si continutul lor)
*/
Interface MyInterface {
	public function myMethod($myParam);
	public function smth();
}

/*
Clasa Abstracta
- proprietati (publice si protected)
- metode 
- constante
- metode abstracte
- nu se poate face obiect dintr-o clasa abstracta
*/

Abstract Class MyAbsClass {
	public $property;
	public function myMethod(){
		return "Hello World!";
	}
	abstract public function something();
} 

Class Ceva extends MyAbsClass {
	public function something(){

	}
}
$object = new Ceva();
$object->property;
$object->myMethod();
$object->something();
