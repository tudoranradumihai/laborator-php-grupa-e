<?php

$a = array();
$a[0] = 10;
$a[1] = 20;

$b = [];
$b[] = 10;
$b[5] = 50;
$b[] = 100;

$c = array(1,2,3);

$d = [1,2,3];

$a[5] = 50;

$e = array(0=>1,1=>2,2=>3);
$e[3] = 4;
$e = array(4=>5,5=>6); 
$e[4] = "VALOARE";

$f = array(
	true,
	1,
	1.5,
	"string"
	);
//var_dump($f);
$g = array(
	0 => "prima valoare",
	1 => "a doua valoare",
	"random" => "random valoare"
);
//var_dump($g);

$matrice = array(
	array(
		"firstname" => "R",
		"lastname" => "T",
		"email" => "..."
	),
	array(
		"firstname" => "L",
		"lastname" => "D",
		"email" => "mue"
	),
	array(
		"firstname" => "K",
		"lastname" => "I",
		"email" => "cool"
	)
);

//echo $matrice[0]["firstname"]." ".$matrice[0]["lastname"];

$temperature = 34.5;
$unit = "C";
$structure = array(
	"C" => array("min"=>35,"max"=>40),
	"F" => array("min"=>85,"max"=>120)
);
var_dump($structure["C"]["min"]);
if (($temperature<$structure["C"]["min"] && $unit=="C" )||($temperature<$structure["F"]["min"] && $unit=="F")){
	
} else if (($temperature>$structure["C"]["max"] && $unit=="C" )||($temperature>$structure["F"]["max"] && $unit=="F")){
	
} else {
	
}