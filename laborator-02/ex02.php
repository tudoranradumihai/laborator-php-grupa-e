<?php
// definirea functiilor 
// http://www.webdevelopmenttraining.ro/training/php/functii/
$a = 1;
/*
function f1(){
	echo $a; // Notice: Undefined variable: a in C:\xampp\htdocs\laborator-php-grupa-e\laborator-02\ex02.php on line 7
}
f1();
*/

function f2($p){
	$p++;
	echo $p."<br>";
}
f2($a);
echo $a."<br>";


// Transmiterea parametrilor prin referinta
$a = 1;
function f3(&$p){
	$p++;
	echo $p."<br>";
}
f3($a);
echo $a."<br>";

// Parametrii cu valori implicite
$a = 2;
$b = 5;
function f4($p1,$p2=1){
	echo $p1**$p2."<br>";
}
f4($a,$b);
f4($a);

// TODO: Transmiterea parametrilor prin "Variable-length argument list"