<?php
// utilizarea variabilelor globale si locale in functii

$a = 1;
/*
function f1(){
	echo $a; // Notice: Undefined variable: a in C:\xampp\htdocs\laborator-php-grupa-e\laborator-02\ex02.php on line 7
}
f1();
*/

function f2(){
	global $a;
	echo $a;
}
f2();