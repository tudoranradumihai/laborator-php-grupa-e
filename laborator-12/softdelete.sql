# create table with soft delete option
CREATE TABLE users (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	# soft delete
	deleted INT(11) DEFAULT 0,
	#
	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	email VARCHAR(255) UNIQUE NOT NULL ,
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP
);

# select data from table with soft delete
SELECT * FROM users WHERE deleted=0;

# delete data from table with soft delete
UPDATE users SET deleted=1 WHERE id=1;
