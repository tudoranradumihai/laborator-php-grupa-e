<?php

$hostname = "localhost";
$username = "root";
$password = "";
$database = "lab12";

// http://php.net/manual/en/function.mysqli-connect.php
$connection = @mysqli_connect($hostname,$username,$password,$database);
if($connection){
	$query = "SELECT cities.id AS 'id',cities.name AS 'city',countries.name AS 'country',continents.name AS 'continent'
	FROM cities
	LEFT JOIN countries
		ON countries.id = cities.country
	LEFT JOIN continents
		ON continents.id = countries.continent;";
	$result = mysqli_query($connection,$query);
	if(mysqli_num_rows($result)>0){
		while($row = mysqli_fetch_assoc($result)){
			echo $row["city"].", ".$row["country"]."<br>";
		}
	}
	mysqli_close($connection);
} else {
	die("ERROR: Could not connect! ".mysqli_connect_error());
}