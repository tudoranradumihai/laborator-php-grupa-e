DROP TABLE IF EXISTS cities;
DROP TABLE IF EXISTS countries;
DROP TABLE IF EXISTS continents;

CREATE TABLE continents (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL
);

CREATE TABLE countries (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	continent INT(11),
	FOREIGN KEY (continent) REFERENCES continents(id)
);

CREATE TABLE cities (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	country INT(11),
	FOREIGN KEY (country) REFERENCES countries(id)
);

INSERT INTO continents (id,name)
	VALUES 
		(1,"Europe"),
		(2,"Asia"),
		(3,"Africa"),
		(4,"Australia"),
		(5,"North America"),
		(6,"South America");

INSERT INTO countries (id,name,continent)
	VALUES
		(1,"Romania",1),
		(2,"Spain",1),
		(3,"Togo",3),
		(4,"Vietnam",2),
		(5,"Australia",4),
		(6,"Bahamas",5);

INSERT INTO cities (id,name,country)
	VALUES
		(1,"Cluj-Napoca",1),
		(2,"Bucharest",1),
		(3,"Brasov",1),
		(4,"Antananarivo",6),
		(5,"Peris",1),
		(6,"Satu Mare",1);

SELECT cities.id AS 'id',cities.name AS 'city',countries.name AS 'country',continents.name AS 'continent'
	FROM cities
	LEFT JOIN countries
		ON countries.id = cities.country
	LEFT JOIN continents
		ON continents.id = countries.continent
	;