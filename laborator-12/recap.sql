# https://www.w3schools.com/sql/sql_create_db.asp
CREATE DATABASE databasename;

# 
DROP DATABASE databasename;

# https://www.w3schools.com/sql/sql_create_table.asp
CREATE TABLE tablename (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	email VARCHAR(255) UNIQUE NOT NULL ,
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP
);

# https://www.w3schools.com/sql/sql_alter.asp
ALTER TABLE tablename
	ADD password VARCHAR(255);

ALTER TABLE tablename
	DROP COLUMN createddate;

ALTER TABLE tablename
	MODIFY COLUMN password VARCHAR(32) NOT NULL;

# https://www.w3schools.com/sql/sql_drop_table.asp
DROP TABLE tablename;

# ---


# CRUD 

# C
# https://www.w3schools.com/sql/sql_insert.asp
INSERT INTO tablename (firstname,lastname,email)
	VALUES ('John',"Doe","john.doe@email.com");

INSERT INTO tablename (firstname,lastname,email)
	VALUES ('John',"Doe","john.doe@email.com"),
	('John',"Doe","john.doe@email.com");

# R
# https://www.w3schools.com/sql/sql_select.asp
SELECT * FROM tablename; 
SELECT firstname,lastname FROM tablename;
SELECT * FROM tablename WHERE id=1;
SELECT * FROM tablename WHERE email LIKE 'john.doe@email.com';
SELECT * FROM tablename ORDER BY lastname ASC;
SELECT * FROM tablename LIMIT 10;
SELECT * FROM tablename LIMIT 10 OFFSET 100;

# U
# https://www.w3schools.com/sql/sql_update.asp
UPDATE tablename
	SET password='1234'
	WHERE id=1;

UPDATE tablename
	SET password='1234'
	WHERE email='john.doe@email.com';

# D
# https://www.w3schools.com/sql/sql_delete.asp
DELETE tablename
	WHERE id=1;
