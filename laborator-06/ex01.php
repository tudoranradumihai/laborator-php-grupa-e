<pre>
<?php

function getURLs($url){	
	if($url[strlen($url)-1]=="/"){
		$url = substr($url,0,-1);
	}
	$HTML = file_get_contents($url);
	$temporary = explode("href=\"",$HTML);
	unset($temporary[0]);

	$array = array();
	foreach($temporary as $value){
		$temp = explode("\"",$value);
		
		if(checkExtension($temp[0])){
			$temp[0] = str_replace("https://","http://",$temp[0]);
			if(strpos($temp[0],"/")===0){
				$temp[0] = $url.$temp[0];
			}
			if(!in_array($temp[0], $array)){
				$array[] = $temp[0];
			}
		}		
	}
	return $array;
}

function checkExtension($url){
	$array = array("ico","jpg","jpeg","png","gif","css","js","pdf");
	$url = explode(".",$url);
	if(in_array($url[count($url)-1],$array) || strpos($url[count($url)-1],"css?")!==false || strpos($url[0],"#")===0){
		return false;
	} else {
		return true;
	}
}

$array = getURLs("http://www.emag.ro/");
ob_start();
$index = 0;
while($index<count($array)){
	echo $array[$index]."<br>";
	ob_flush();
	flush();
	$links = getURLs($array[$index]);
	foreach($links as $link){
		if(!in_array($link,$array)){
			$array[] = $link;
		}
	}
	$index++;
}
//print_r($array);
ob_end_flush();