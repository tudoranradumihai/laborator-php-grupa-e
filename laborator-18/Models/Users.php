<?php
// ETAPA 7.1
Class Users {

	public $id;
	public $firstname;
	public $lastname;
	public $email;
	public $password;

	public static function findAll(){
		// ETAPA 4
		$database = new Database();
		// ETAPA 6
		$result = $database->query("SELECT * FROM users");
		$users = array();
		if (mysqli_num_rows($result)>0){
			while ($line = mysqli_fetch_assoc($result)){
				// ETAPA 7
				$user = new Users();
				foreach($user as $key => $value){
					$user->$key = $line[$key];
				}
				// ETAPA 8
				array_push($users, $user);
			}
		}
		// ETAPA 9
		return $users;
	}

}