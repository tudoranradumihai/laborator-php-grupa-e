<a href="index.php">Home</a>
<a href="index.php?C=Users&A=new">New User</a>
<a href="index.php?C=Users&A=list">List Users</a>
<br>
<?php
require_once "Helpers/Autoload.php";

// ETAPA 1
if(array_key_exists("C", $_GET)){
	$controller = $_GET["C"]."Controller";
} else {
	$controller = "DefaultController";
}
if(array_key_exists("A", $_GET)){
	$action = $_GET["A"]."Action";
} else {
	$action = "defaultAction";
}

if(class_exists($controller)){
	$object = new $controller();
	if(method_exists($object, $action)){
		// ETAPA 2
		$object->$action();
	} else {
		/* TEMPORARY - TO DO: LOG Class to add error messages */
		echo "Method '$controller::$action' doesn't exist.<br>";
	}
} else {
	/* TEMPORARY - TO DO: LOG Class to add error messages */
	echo "Class '$controller' doesn't exist.<br>";
}
?>
