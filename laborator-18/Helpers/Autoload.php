<?php
function __autoload($class){
	$folders = array("Controllers","Models","Helpers");
	foreach($folders as $folder){
		$path = "$folder/$class.php";
		if(file_exists($path)){
			require $path;
		}
	}
}