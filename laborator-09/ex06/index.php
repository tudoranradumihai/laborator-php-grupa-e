<?php
	if($_SERVER["REQUEST_METHOD"]=="POST"){
		if(!file_exists("files")){
			mkdir("files");
		}
		$filename = "files/".$_POST["filename"].".txt";
		$file = fopen($filename,"w");  
		fputs($file,$_POST["content"].PHP_EOL);
		fclose($file);
	}
?>
<form method="POST">
	<div>
		<label>
			Filename
		</label>
		<input type="text" name="filename" />
	</div>
	<div>
		<label>
			Content
		</label>
		<textarea name="content"></textarea>
	</div>
	<div>
		<input type="submit">
	</div>
</form>
<?php
$files = scandir("files");
$files = array_diff($files,array(".",".."));
foreach($files as $file){
	echo "<fieldset>";
	echo "<p>$file</p>";
	echo "<hr>";
	echo file_get_contents("files/".$file);
	echo "</fieldset>";
}