<?php
/*
http://php.net/manual/en/function.file-exists.php
file_exists(filename)
*/

if(file_exists("text.txt")){
	echo "Exists";
} else {
	echo "Doesn't exist";
}
echo "<br>";

if(file_exists("subfolder")){
	echo "Exists";
} else {
	echo "Doesn't exist";
}
echo "<br>";

$structure = scandir("../ex03");
var_dump($structure);