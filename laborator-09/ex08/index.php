<?php

/*
formular cu campurile firstname, lastname si email
la executia formularului, datele venite din formular se vor salva intr-un fisier users.txt pe o singura linie in format json

- verificati daca vine din post
- sa faceti un array cu firstname=... lastname=... email=..
- sa encodati in json acest array
- sa deschideti fisierul users.txt
- sa scrieti encodul in fisier
- sa inchideti fisierul
*/

if($_SERVER["REQUEST_METHOD"]="POST"){
	$array = array(
		"firstname"=>$_POST["firstname"],
		"lastname"=>$_POST["lastname"],
		"email"=>$_POST["email"],
		);
	$array = json_encode($array);
	$file = fopen("users.txt","a+");
	fputs($file,$array.PHP_EOL);
	fclose($file);
}