<?php
/*
Formular GET cu field search care cauta utilizatorii din fisier si afiseaza utilizatorii daca email-ul contine bucata de text cautata
*/
if(array_key_exists("search", $_GET)){
	$search = $_GET["search"];
} else {
	$search = "";
}

?>
<form method="GET">
	<input type="text" name="search" value="<?php echo $search ?>" />
	<input type="submit">
</form>
<?php
if($search!=""){
	$file = fopen("users.txt","r");
	while($line = fgets($file)){
		$user = json_decode($line,TRUE);
		if(stripos($user["email"],$search)!==false){
			echo $user["firstname"]." ".$user["lastname"]."<br>";
		}
	}
}